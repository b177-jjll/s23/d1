// select database
use <database name>

// when creating a new database via the command line, the use command line can be entered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created.

// database = filing cabinet
// collection = drawer
// document/record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file/folder content

/* e.g. document with no sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}
*/

/* e.g. document with sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		adress: {
			street: "123 Street st",
			city: "Makati",
			country: "Philippines"
		}
	}
*/

/*
	Embedded vs. Referenced data:

	Referenced Data
	Users:
		{
			id : 300,
			name: "Jino Yumul",
			age: 33,
			occupation: "Instructor"
		}

	Orders:
		{
			products: [
				{
					name: "New pillow",
					price: 300
				}
			],
			userId: 300
		}

	Embedded Data
	Users:
		{
			id : 300,
			name: "Jino Yumul",
			age: 33,
			occupation: "Instructor",
			orders: [
				{
					products: [
						{
							name: "New pillow",
							price: 300
						}
					]
				}
			]
		}
*/

// Insert One Document (Create)

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// if inserting/creating a new document within a collection that does not yet exist, MongoDB will automatically create that collection

// insert many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

// Finding Documents (Read)

// Retrieve a list of ALL users
db.users.find()

// Find a specific user

// Finds any and all matches
db.users.find(
	{firstName: "Stephen"}
)
// Finds only the first match
db.users.findOne(
	{firstName: "Stephen"}
)

// Find documents with multiple parametes/conditions
db.users.find(
	{lastName: "Armstrong",
	age: 82}
)

// Update/Edit Documents

// Create a new document to update
db.users.insert({
	firstName : "Test",
	lastName : "Test",
	age : 0,
	contact : {
		phone : "123456789",
		email : "test@mail.com"
	},
	courses : [],
	department : "none"
})

// Update/edit
// updateOne always need to be provided two objects. The first object specifies WHICH document to update. The second objects contains the $set operator, and inside the $set operator are the specific fields to be updated.
db.users.updateOne({_id : ObjectId("62876c3582da15434ec6b379")},
	{
		$set: {
			firstName : "Bill",
			lastName : "Gates",
			age : 65,
			contact : {
				phone : "123456789",
				email : "billgates@mail.com"
			},
			courses : ["PHP", "Laravel", "HTML"],
			department : "Operations"
		}
	}
)